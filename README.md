# WEB Font Setting on Appearance Menu

## Notebooks
[QA_T350709.ipynb](https://gitlab.wikimedia.org/jiawang/web_appearance_menu/-/blob/master/QA_T350709.ipynb?ref_type=heads)
- QA instrumentation

[01_T359418_font_desktopweb_R.ipynb](https://gitlab.wikimedia.org/jiawang/web_appearance_menu/-/blob/master/01_T359418_font_desktopweb_R.ipynb?ref_type=heads)
- Analysis for font menu beta deployment on logged-in users
- Code for result summary of https://phabricator.wikimedia.org/T359418#9694334


[02_T359418_font_beta_user_python.ipynb](https://gitlab.wikimedia.org/jiawang/web_appearance_menu/-/blob/master/02_T359418_font_beta_user_python.ipynb?ref_type=heads)
- Estimated number of users who enabled beta feature
- Code for result summary of https://phabricator.wikimedia.org/T359418#9694334


[04_T364483_font_desktopweb_R.ipynb](https://gitlab.wikimedia.org/jiawang/web_appearance_menu/-/blob/master/04_T364483_font_desktopweb_R.ipynb?ref_type=heads)
- Analysis for font menu deployment on pilot wikis for both logged-in users and anonymous users
- Code for result summary of https://phabricator.wikimedia.org/T364483#9818012
